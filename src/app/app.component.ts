import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent  implements OnInit {

  dropdownOptions = ["Basic", "Advanced", "Pro"];

  defaultValue = 'Advanced';

  ngOnInit() {

  }

  onSubmit(form: NgForm) {
    console.log('aaaa');
    console.log(form);
  }
}
